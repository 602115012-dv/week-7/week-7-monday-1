import React from 'react';
import SideProfile from './SideProfile';

const Preference = () => {
  return(
    <div className="row justify-content-center">
      <div className="col-8">
        <div className="container" id="two-content">
          <div className="row">
            <SideProfile />

            <div className="col-8 offset-md-1">
              <div className="row">
                <div className="col header">
                  <h1>Preference</h1>
                  <hr />
                </div>
                <div className="w-100"></div>
                <div className="col">
                  <div className="row">
                    <div className="col">
                      <div className="row">
                        <div className="card mb-3">
                          <div className="row no-gutters box-shadow">
                            <div className="col-md-4">
                              <img src="/assets/stratocaster.jpg" className="card-img" alt="..." />
                            </div>
                            <div className="col-md-8">
                              <div className="card-body">
                                <h5 className="card-title">Guitar</h5>
                                <p className="card-text">
                                  I think guitar makes guitarist cool when they play it well.
                                  It is a versitile instrument.
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col">
                      <div className="row">
                        <div className="card mb-3">
                          <div className="row no-gutters box-shadow">
                            <div className="col-md-4">
                              <img src="/assets/cooking_in_kitchen.jpg" className="card-img" alt="..." />
                            </div>
                            <div className="col-md-8">
                              <div className="card-body">
                                <h5 className="card-title">Cooking</h5>
                                <p className="card-text">
                                  I think guitar makes guitarist cool when they play it well.
                                  It is a versitile instrument.
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Preference;