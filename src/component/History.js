import React from 'react';
import SideProfile from './SideProfile';

const History = () => {
  return (
    <div className="row justify-content-center">
      <div className="col-8">
        <div className="container" id="two-content">
          <div className="row">
            <SideProfile />

            <div className="col-8 offset-md-1">
              <div className="row">
                <div className="col header">
                  <h1>History</h1>
                  <hr />
                </div>
                <div className="w-100"></div>
                <div className="col">
                  <div className="row">
                    <div className="col">
                      <div className="row">
                        <div className="card mb-3">
                          <div className="row no-gutters box-shadow">
                            <div className="col-md-4">
                              <img src="/assets/show_pro_x.jpg" className="card-img" alt="..." />
                            </div>
                            <div className="col-md-8">
                              <div className="card-body">
                                <h5 className="card-title">Show pro X</h5>
                                <div className="card-text">
                                  <div>
                                    <b>Date</b>: 14 Nov 2018
                                  </div>
                                  <div>
                                    <b>Place</b>: CAMT
                                  </div>
                                  <div>
                                    <b>My duty</b>: Preparing the room for the event before the event day.
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col">
                      <div className="row">
                        <div className="card mb-3">
                          <div className="row no-gutters box-shadow">
                            <div className="col-md-4">
                              <img src="/assets/openhouse.jpg" className="card-img" alt="..." />
                            </div>
                            <div className="col-md-8">
                              <div className="card-body">
                                <h5 className="card-title">CAMT Openhouse</h5>
                                <div className="card-text">
                                  <div>
                                    <b>Date</b>: 8 Nov 2018
                                  </div>
                                  <div>
                                    <b>Place</b>: CAMT
                                  </div>
                                  <div>
                                    <b>My duty</b>: Greeting arriving student and answering question
                                    about my major (SE).
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <div className="row">
                    <div className="col">
                      <div className="row">
                        <div className="card mb-3">
                          <div className="row no-gutters box-shadow">
                            <div className="col-md-4">
                              <img src="/assets/24hr.jpg" className="card-img" alt="..." />
                            </div>
                            <div className="col-md-8">
                              <div className="card-body">
                                <h5 className="card-title">24 Hours of Innovation : Northern region</h5>
                                <div className="card-text">
                                  <div>
                                    <b>Date</b>: 15-16 Sep 2018
                                  </div>
                                  <div>
                                    <b>Place</b>: CMU STeP
                                  </div>
                                  <div>
                                    <b>My duty</b>: Competing in the event and got third place.
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default History;