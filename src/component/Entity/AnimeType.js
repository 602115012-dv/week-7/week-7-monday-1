const AnimeType = {
  ALL: 'All',
  TV: 'TV',
  OVA: 'OVA',
  MOVIE: 'Movie',
  SPECIAL: 'Special',
  ONA: 'ONA',
  MUSIC: 'Music'
}

export default AnimeType;