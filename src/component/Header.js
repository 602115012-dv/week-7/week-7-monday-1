import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

const Header = () =>  {
  return(
    <div className="container" id="navbar">
      <div className="row align-items-center">
        <div className="col">
          <img id="logo" src="/assets/camt_horizontal.png" alt="" align="left" />
        </div>

        <div className="w-100"></div>

        <div className="col" >
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="/home">Home</Nav.Link>
              <Nav.Link href="/preference">Preference</Nav.Link>
              <Nav.Link href="/specialty">Specialty</Nav.Link>
              <Nav.Link href="/history">History</Nav.Link>
              <Nav.Link href="/anime">Anime</Nav.Link>
              <Nav.Link href="/education">Education</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        </div>
      </div>
    </div>
  );
}

export default Header;