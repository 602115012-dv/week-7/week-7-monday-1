import React from 'react';
import SideProfile from './SideProfile';

const Specialty = () => {
  return (
    <div className="row justify-content-center">
    <div className="col-8">
      <div className="container" id="two-content">
        <div className="row">
          <SideProfile />

          <div className="col-8 offset-md-1">
            <div className="row">
              <div className="col header">
                <h1>Specialty</h1>
                <hr />
              </div>
              <div className="w-100"></div>
              <div className="col" id="side-content">
                <div className="row">                
                  <div className="w-100"></div>
        
                  <div className="col">
                    <h2>Technical skills</h2>
                  </div>
        
                  <div className="w-100"></div>
        
                  <div className="col side-margin">
                    <div className="row">
                      <div className="col">
                        <h3>Programming language</h3>
                      </div>
        
                      <div className="w-100"></div>
        
                      <div className="col">
                        <div className="row">
                          <div className="col text-align-center">
                            <div>
                              <img className="skill-img" src="/assets/java_logo.png" alt="" />
                            </div>
                            <div>
                              Java
                            </div>
                          </div>
                          <div className="col text-align-center">
                            <div>
                              <img className="skill-img" src="/assets/javascript_logo.jpg" alt="" />
                            </div>
                            <div>
                              Javascript
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <div className="w-100"></div>
                  
                  <div className="col side-margin">
                    <div className="row">
                      <div className="col">
                        <h3>Others</h3>
                      </div>
        
                      <div className="w-100"></div>
        
                      <div className="col">
                        <div className="row">
                        <div className="col text-align-center">
                            <div>
                              <img className="skill-img" src="/logo192.png" alt="" />
                            </div>
                            <div>
                              React
                            </div>
                          </div>

                          <div className="col text-align-center">
                            <div>
                              <img className="skill-img" src="/assets/html5_logo.png" alt="" />
                            </div>
                            <div>
                              HTML5
                            </div>
                          </div>
        
                          <div className="col text-align-center">
                            <div>
                              <img className="skill-img" src="/assets/css3_logo.png" alt="" />
                            </div>
                            <div>
                              CSS3
                            </div>
                          </div>
        
                          <div className="col text-align-center">
                            <div>
                              <img className="skill-img" src="/assets/bootstrap_logo.png" alt="" />
                            </div>
                            <div>
                              Bootstrap
                            </div>
                          </div>
        
                          <div className="col text-align-center">
                            <div>
                              <img className="skill-img" src="/assets/mysql_logo.png" alt="" />
                            </div>
                            <div>
                              SQL
                            </div>
                          </div>
        
                          <div className="col text-align-center">
                            <div>
                              <img className="skill-img" src="/assets/git_logo.png" alt="" />
                            </div>
                            <div>
                              Git
                            </div>
                          </div>
        
                          <div className="col text-align-center">
                            <div>
                              <img className="skill-img" src="/assets/linux_logo.jpg" alt="" />
                            </div>
                            <div>
                              Linux
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
        
                  <div className="w-100"></div>
        
                  <div className="col">
                    <div className="row">
                      <div className="col">
                        <h2>Languages</h2>
                      </div>
        
                      <div className="w-100"></div>
        
                      <div className="col side-margin">
                        <div>
                          <img src="/assets/thailand_flag.png" alt="" /> 
                          <span>
                            Thai
                          </span>
                          <span>
                            <img src="/assets/checked_star.png" alt="" />
                            <img src="/assets/checked_star.png" alt="" />
                            <img src="/assets/checked_star.png" alt="" />
                            <img src="/assets/checked_star.png" alt="" />
                            <img src="/assets/checked_star.png" alt="" />
                          </span>
                        </div>
                      </div>
        
                      <div className="w-100"></div>
        
                      <div className="col side-margin">
                        <div>
                          <img src="/assets/usa_flag.png" alt="" /> 
                          <span>
                            English
                          </span>
                          <span>
                            <img src="/assets/checked_star.png" alt="" />
                            <img src="/assets/checked_star.png" alt="" />
                            <img src="/assets/checked_star.png" alt="" />
                            <img src="/assets/empty_star.png" alt="" />
                            <img src="/assets/empty_star.png" alt="" />
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  );
}

export default Specialty;