import React from 'react';

const Homepage = () => {
  return(
    <div className="row justify-content-center">
      <div className="col-6">
        <div className="container" id="content">
          <div className="row">
            <div className="col-12">     
              <div id="bio">
                <img src="/assets/602115012_pic.jpg" alt="" />
              </div>
            </div>
            <div className="col-12">
              <div className="mainInfo">
                <p>
                  <b>Thitiwut Chutipongwanit</b>
                </p>
                <p>
                  <img src="/assets/icon_student.png" alt="" />
                  Student, 
                  <img src="/assets/icon_thailand_32px.png" alt="" />
                  Thai, 
                  <img src="/assets/icon_monk_32px.png" alt="" />
                  Buddhist, and 23
                </p>
              </div>
              <p>
                <img src="/assets/icon_github.png" alt="" height="24" />
                <b>Github</b>: 
                <a href="https://github.com/thitiwut-chu?tab=repositories">
                  https://github.com/thitiwut-chu?tab=repositories 
                </a> 
              </p>
            </div>

            <div className="col-12">    
              <hr /> 
              <h2><u>Contact</u></h2>
              <p>
                <img src="/assets/icon_mail.png" alt="" />
                <b>Email</b>: thiwfirst@gmail.com
              </p>
              <p>
                <img src="/assets/icon_call.png" alt="" />
                <b>Phone number</b>: +66 85-030-5133
              </p>
              <p>
                <img src="/assets/icon_home.png" alt="" />
                <b>Current residence</b>: Chiang Mai, Thailand
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Homepage;