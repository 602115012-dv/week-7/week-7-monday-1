import React from 'react';
import { Row, Col } from 'antd';

const SideProfile = () => {
  return (
    <Col span={6} id="side-profile-container">
      <Row id="side-profile">
        <Col span={24}>
          <img src="/assets/602115012_pic.jpg" alt="" />
        </Col>
        <Col span={24}>
          <div>
            <p>
              <b>Thitiwut Chutipongwanit</b>
            </p>
            <p>
              <img src="/assets/icon_student_24px.png" alt="" />
              Student, 
              <img src="/assets/icon_thailand_24px.png" alt="" />
              Thai, 
              <img src="/assets/icon_monk_24px.png" alt="" />
              Buddhist, and 23
            </p>
          </div>
        </Col>
      </Row>
    </Col>
  );
}

export default SideProfile;