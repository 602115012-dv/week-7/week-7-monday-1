import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import SideProfile from './SideProfile';

const Education = () => {
  const [academicRecords, setAcademicRecords] = useState([
    {
      "year": "1/2560",
      "records": [
        {
          "courseId": "001101",
          "courseTitle": "FUNDAMENTAL ENGLISH 1",
          "courseCredit": 3.00,
          "courseGrade": "CX"
        },
        {
          "courseId": "001102",
          "courseTitle": "FUNDAMENTAL ENGLISH 2",
          "courseCredit": 3.00,
          "courseGrade": "CX"
        },
        {
          "courseId": "001201",
          "courseTitle": "CRIT READ AND EFFEC WRITE",
          "courseCredit": 3.00,
          "courseGrade": "CX"
        },
        {
          "courseId": "206113",
          "courseTitle": "CAL FOR SOFTWARE ENGINEERING",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "211100",
          "courseTitle": "EATING WELL : BET LIV & DIS PRE",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "259109",
          "courseTitle": "TELECOM IN THAILAND",
          "courseCredit": 3.00,
          "courseGrade": "CX"
        },
        {
          "courseId": "751100",
          "courseTitle": "ECONOMICS FOR EVERYDAY LIFE",
          "courseCredit": 3.00,
          "courseGrade": "CX"
        },
        {
          "courseId": "951100",
          "courseTitle": "MODERN LIFE AND ANIMATION",
          "courseCredit": 3.00,
          "courseGrade": "B+"
        },
        {
          "courseId": "953103",
          "courseTitle": "PROGRAMMING LOGICAL THINKING",
          "courseCredit": 2.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953211",
          "courseTitle": "COMPUTER ORGANIZATION",
          "courseCredit": 3.00,
          "courseGrade": "A"
        }
      ],
      "display": true
    },
    {
      "year": "2/2560",
      "records": [
        {
          "courseId": "011251",
          "courseTitle": "LOGIC",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953102",
          "courseTitle": "ADT & PROBLEM SOLVING",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953104",
          "courseTitle": "WEB UI DESIGN & DEVELOP",
          "courseCredit": 2.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953202",
          "courseTitle": "INTRODUCTION TO SE",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953231",
          "courseTitle": "OBJECT ORIENTED PROGRAMMING",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "955100",
          "courseTitle": "LEARNING THROUGH ACTIVITIES 1",
          "courseCredit": 1.00,
          "courseGrade": "A"
        }
      ],
      "display": true
    },
    {
      "year": "1/2561",
      "records": [
        {
          "courseId": "206281",
          "courseTitle": "DISCRETE MATHEMATICS",
          "courseCredit": 3.00,
          "courseGrade": "B+"
        },
        {
          "courseId": "953212",
          "courseTitle": "DB SYS & DB SYS DESIGN",
          "courseCredit": 3.00,
          "courseGrade": "B+"
        },
        {
          "courseId": "953233",
          "courseTitle": "PROGRAMMING METHODOLOGY",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953261",
          "courseTitle": "INTERACTIVE WEB DEVELOPMENT",
          "courseCredit": 2.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953361",
          "courseTitle": "COMP NETWORK & PROTOCOLS",
          "courseCredit": 3.00,
          "courseGrade": "C"
        }
      ],
      "display": true
    },
    {
      "year": "2/2561",
      "records": [
        {
          "courseId": "001225",
          "courseTitle": "ENGL IN SCIENCE & TECH CONT",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "206255",
          "courseTitle": "MATH FOR SOFTWARE TECH",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953201",
          "courseTitle": "ALGO DESIGN & ANALYSIS",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953214",
          "courseTitle": "OS & PROG LANG PRINCIPLES",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953232",
          "courseTitle": "OO ANALYSIS & DESIGN",
          "courseCredit": 3.00,
          "courseGrade": "B+"
        },
        {
          "courseId": "953234",
          "courseTitle": "ADVANCED SOFTWARE DEVELOPMENT",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "955200",
          "courseTitle": "LEARNING THROUGH ACTIVITIES 2",
          "courseCredit": 1.00,
          "courseGrade": "B"
        }
      ],
      "display": true
    },
    {
      "year": "1/2562",
      "records": [
        {
          "courseId": "011151",
          "courseTitle": "REASONING",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "208263",
          "courseTitle": "ELEMENTARY STATISTICS",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "269111",
          "courseTitle": "COMMU TECH IN A CHANGING WORLD",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "888102",
          "courseTitle": "BIG DATA FOR BUSINESS",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953321",
          "courseTitle": "SOFTWARE REQ ANALYSIS",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953322",
          "courseTitle": "SOFTWARE DESIGN & ARCH",
          "courseCredit": 3.00,
          "courseGrade": "A"
        },
        {
          "courseId": "953331",
          "courseTitle": "COMPO-BASED SOFTWARE DEV",
          "courseCredit": 3.00,
          "courseGrade": "A"
        }
      ],
      "display": true
    }
  ]);

  const renderGradeTable = () => {
    return academicRecords.map((semester, index) => {
      if (semester.display === false) {
        return;
      }
      return (
        <div className="col gradeTable" key={index}>
          <h3>Semester {semester.year}</h3>
          <table className="table table-striped table-dark td-text-align-left">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Course Id</th>
                <th scope="col">Course Title</th>
                <th scope="col">Credit</th>                                    
                <th scope="col">Grade</th>
              </tr>
            </thead>
            <tbody>
              {semester.records.map((course, index) => {
                return (
                  <tr key={index}>
                    <th scope="row">{index + 1}</th>
                    <td>{course.courseId}</td>
                    <td>{course.courseTitle}</td>
                    <td>{course.courseCredit}</td>
                    <td>{course.courseGrade}</td>
                  </tr>
                 );
                })
              }
            </tbody>
          </table>
        </div>
      );
    });
  }

  const filterGradeTable = (event, year) => {
    let index = academicRecords.findIndex((semester) => {return year === semester.year});
    let tempAcademicRecords = [...academicRecords];   
    tempAcademicRecords[index].display = event.target.checked;
    setAcademicRecords(tempAcademicRecords);
  }

  const checkAll = () => {
    let tempAcademicRecords = [...academicRecords]
    tempAcademicRecords.forEach(element => {
      element.display = true;
    });
    setAcademicRecords(tempAcademicRecords);
  }

  const uncheckAll = () => {
    let tempAcademicRecords = [...academicRecords]
    tempAcademicRecords.forEach(element => {
      element.display = false;
    });
    setAcademicRecords(tempAcademicRecords);

  }

  return (
    <div className="row justify-content-center">
      <div className="col-8">
        <div className="container" id="two-content">
          <div className="row">
            <SideProfile />
            <div className="col-8 offset-md-1">
              <div className="row">
                <div className="col header">
                  <h1>Education</h1>
                  <hr />
                </div>
                <div className="w-100"></div>
                <div className="col" id="side-content">
                  <div className="row">                
                    <div className="w-100"></div>
                    <div className="col">
                      <div className="row">
                        <div className="col">
                          <b>College of art, Media and Technology Chiang Mai University</b>
                          <p>
                            Bachelor of science, Software Engineering major
                          </p>
                          <p>
                            August 2017 - March 2021 (expected)
                          </p>
                          <p>
                            Current cumulative GPA (5 semesters): 3.84
                          </p>
                        </div> 
                      </div>                  
                    </div>
                  </div>
                </div>

                <div className="w-100"></div>

                <div className="col margin-top-2" id="side-content">
                  <div className="row">                
                    <div className="w-100"></div>
                    <div className="col">
                      <div className="row">
                        <div className="w-100"></div>
                        <div className="col">
                          <div className="row">
                            <div className="col text-align-center">
                              <h2>
                                <img src="/assets/icon_cap_32px.png" alt="" />
                                Academic performance
                              </h2>
                              <hr />
                            </div>
                            <div className="w-100"></div>
                            <Form style={{margin: "0 15px"}}>
                              {['checkbox'].map(type => (
                                <div key={`default-${type}`} className="mb-3">
                                  {academicRecords.map((semester, index) => {
                                    return (
                                      <Form.Check key={index} checked={semester.display} 
                                      label={`Semester ${semester.year}`} type={type} 
                                      onChange={
                                        (event) => filterGradeTable(event, semester.year)
                                      }/>
                                    );
                                  })}
                                </div>
                              ))}
                            </Form>
                            <div className="w-100"></div>
                            <ButtonToolbar style={{margin: "0 15px"}}>
                              <Button variant="primary" onClick={checkAll}>Check all</Button>
                            </ButtonToolbar>
                            <div className="w-100"></div>
                            <ButtonToolbar style={{margin: "0 15px"}}>
                              <Button variant="danger" onClick={uncheckAll}>Uncheck all</Button>
                            </ButtonToolbar>
                            <div className="w-100"></div>
                            {renderGradeTable()}
                          </div>
                        </div>
                      </div>                  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Education;