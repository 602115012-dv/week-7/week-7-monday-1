import React, { useState, useEffect } from 'react';
import SideProfile from './SideProfile';
import { Form, Button, Input, Spin, Icon, Row, Col, Typography, Pagination, Select } from 'antd';
import AnimeType from './Entity/AnimeType';

const Anime = () => {
  const [animes, setAnimes] = useState([]);
  const [outputAnimes, setOutputAnimes] = useState([]);
  const [isLoading, setIsLoading] = useState();
  const [totalAnimes, setTotalAnimes] = useState();
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [animeType, setAnimeType] = useState(AnimeType.ALL);
  const antIcon = <Icon type="loading" style={{ fontSize: 100 }} spin />;
  const { Title } = Typography;
  const { Text } = Typography;
  const { Search } = Input;
  const { Option } = Select;

  useEffect(() => {
    (async () => {
      setIsLoading(true);
      try {
        let response = await fetch("https://api.jikan.moe/v3/anime/235");
        let data = await response.json();
        let conan = [
          {
            "title": data["title"],
            "synopsis": data["synopsis"],
            "image_url": data["image_url"],
            "url": data["url"],
            "type": data["type"]
          }
        ]
        setTotalAnimes(1);
        setAnimes(conan);
        setOutputAnimes(conan);
      } catch (err) {
        console.log(err);
      }
      setIsLoading(false);
    })();
  }, []);

  const fetchAnime = (animeTitle) => {
    setAnimes([]);
    setOutputAnimes([]);
    setIsLoading(true);
    (async () => {
      try {
        let response = await fetch(`https://api.jikan.moe/v3/search/anime?q=${animeTitle}`);
        let data = await response.json();
        if (data["results"].length === 0) {
          setAnimes([]);
          setOutputAnimes([]);
          setIsLoading(false);
        } else {
          setAnimes(data["results"]);
          setOutputAnimes(data["results"]);
          setIsLoading(false);
        }
        setTotalAnimes(data["results"].length);
      } catch (err) {
        console.log(err);
        setAnimes(-1);
        setOutputAnimes(-1)
        setIsLoading(false);
        setTotalAnimes(-1);
      }
    })();
  }

  const isInRange = (max, min, num) => {
    if (num <= max && num >= min) {
      return true;
    }
    return false;
  }

  const renderAnimeData = () => {
    if (outputAnimes.length === 0 && !isLoading) {
      return (
        <div>No results were found.</div>
      );
    }

    if (outputAnimes === -1) {
      return (
        <div>Server error. Please try again.</div>
      );
    }

    let maxIndex = currentPage * pageSize - 1;
    let minIndex = currentPage * pageSize - pageSize;

    return outputAnimes.map((anime, index) => {
      if (!isInRange(maxIndex, minIndex, index)) {
        return null;
      }

      return (
        <Row key={index}>
          <Col>
            <Row>
              <div className="card mb-3">
                <div className="row no-gutters box-shadow">
                  <div className="col-md-4">
                    <img className="card-img" src={anime["image_url"]} />
                  </div>
                  <div className="col-md-8">
                    <div className="card-body">
                      <Title level={2} className="card-title">{anime["title"]}</Title>
                      <hr />
                      <div>
                        <b>Type: </b><Text code>{anime["type"]}</Text>
                      </div>
                      <hr />
                      <div>
                        <Title level={3}>Synopsis</Title>
                      </div>
                      <p className="card-text">
                        {anime["synopsis"]}
                      </p>
                      <div style={{textAlign: "right"}}>
                        <Button type="link" size={"small"}>
                          <a href={anime["url"]} target="_blank">
                          Read more
                          </a>
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Row>
          </Col>
        </Row>
      );
    });
  }

  const changePage = (page, pageSize) => {
    setCurrentPage(page);
    window.scrollTo(0, 0);
  }

  const changePageSize = (current, size) => {
    setCurrentPage(current);
    setPageSize(size);
    window.scrollTo(0, 0);
  }

  const filterAnimeType = (animeType) => {
    setAnimeType(animeType);

    //Set the pagination size
    let size = 0;
    if (animeType !== AnimeType.ALL) { 
      for (let i = 0; i < outputAnimes.length; i++) {
        if (animeType === animes[i].type)
          size++;
      }
    } else {
      size = outputAnimes.length;
    }
    setTotalAnimes(size);

    //Create new array with only anime that is the selected anime type
    //and assign it to outputAnimes
    let tempAnimes = [];
    if (animeType === AnimeType.ALL) {
      setOutputAnimes([...animes]);
    } else {
      for (let i = 0; i < outputAnimes.length; i++) {
        if (animes[i].type === animeType) {
          tempAnimes.push(animes[i]);
        }
      }
      setOutputAnimes(tempAnimes);
    }
  }

  return (
    <Row type="flex" justify="center">
      <Col span={16}>
        <div className="container" id="two-content">
          <Row>
            <SideProfile />
            <Col span={16} offset={2}>
              <Row>
                <Col className="header">
                  <Title>Anime</Title>
                  <hr />
                </Col>
                <Col>
                  <Form layout="inline"
                  onSubmit={(event) => event.preventDefault()} style={{marginBottom: "5px"}}>
                    <Form.Item>
                      <Search
                        placeholder="Anime title"
                        enterButton="Search"
                        onSearch={value => fetchAnime(value)}
                      />
                    </Form.Item>
                    <Form.Item>
                      <Select defaultValue="All" style={{ width: 120 }} onChange={(value) => filterAnimeType(value)}>
                        <Option value={AnimeType.ALL}>{AnimeType.ALL}</Option>
                        <Option value={AnimeType.TV}>{AnimeType.TV}</Option>
                        <Option value={AnimeType.OVA}>{AnimeType.OVA}</Option>
                        <Option value={AnimeType.MOVIE}>{AnimeType.MOVIE}</Option>
                        <Option value={AnimeType.SPECIAL}>{AnimeType.SPECIAL}</Option>
                        <Option value={AnimeType.ONA}>{AnimeType.ONA}</Option>
                        <Option value={AnimeType.MUSIC}>{AnimeType.MUSIC}</Option>
                      </Select>
                    </Form.Item>
                  </Form>
                </Col>
                <Col span={24} hidden={!isLoading}>
                  <div className="flex flex-center" style={{ margin: "5% 0" }}>
                    <Spin indicator={antIcon} />
                  </div>
                </Col>
                <Col span={24} id="placeholder">
                  {renderAnimeData()}
                  <Pagination 
                  defaultCurrent={1}
                  total={totalAnimes} 
                  showSizeChanger onChange={(page, pageSize) => changePage(page, pageSize)} 
                  onShowSizeChange={(current, size) => changePageSize(current, size)} />
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </Col>
    </Row>
  );
}

export default Anime;