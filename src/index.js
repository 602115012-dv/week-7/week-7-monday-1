import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import Header from './component/Header';
import Homepage from './component/Homepage';
import Preference from './component/Preference';
import Specialty from './component/Specialty';
import './component/style.css';
import History from './component/History';
import Education from './component/Education';
import Anime from './component/Anime';
import 'antd/dist/antd.css';

const MainRouting = 
  <BrowserRouter>
    <Route exact path="/" render={() => (
      <Redirect to="/home"/>
    )}/>
    <Route path="/" component={Header}/>
    <Route path="/home" component={Homepage} />
    <Route path="/preference" component={Preference} />
    <Route path="/specialty" component={Specialty} />
    <Route path="/history" component={History} />
    <Route path="/education" component={Education} />
    <Route path="/anime" component={Anime} />
  </BrowserRouter>
  
ReactDOM.render(MainRouting, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
